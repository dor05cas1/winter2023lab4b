public class Panda{
	
	private String food;
	private double sizeInCM;
	private int age;
	
	//Added constructor 
	public Panda(String food, double sizeInCM, int age){
		this.food = food;
		this.sizeInCM = sizeInCM;
		this.age = age;
	}
	
	//public void setFood(String food){
		//this.food = food;
	//}
	
	//public void setSizeInCM(double sizeInCM){
		//this.sizeInCM = sizeInCM;
	//}
	
	//Set methods...
	public void setAge(int Age){
		this.age = age;
	}
	
	public String getFood(){
		return this.food;
	}
	
	//Get methods
	public double getSizeInCM(){
		return this.sizeInCM;
	}
	
	public int getAge(){
		return this.age;
	}
	
	public void printLifeSpan(){
		
		if(age >= 20){
			System.out.println("Your panda is dead or waiting for death");
		}
		else if( age > 10){
			System.out.println("Your panda is an full adult waiting to breed");
		}
		else{
			System.out.println("Your panda is a youngling");
		}
		
	}
	
	public void eatBamboo(){
		if(food.equalsIgnoreCase("Bamboo")){
			System.out.println("Your Panda is basic");
		}
		else if(food.equalsIgnoreCase("Leaves")){
			
			System.out.println("Your Panda is suffering! Give it Bamboo");
		}
		else{
			System.out.println("Your Panda has died");
		}
		
	}
}
