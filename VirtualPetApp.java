import java.util.Scanner;
public class VirtualPetApp{
	
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		Panda[] embarrassment = new Panda[1];
		
		for(int i =0; i < embarrassment.length; i++){
			
			System.out.print("What is panda " + (i+1) + " favorite food: ");
			String food = input.nextLine();
			
			
			System.out.print("What is panda " + (i+1) + " size in CM: ");
			double sizeInCM = Double.parseDouble(input.nextLine());
			
			
			System.out.print("What is panda " + (i+1) + " age? ");
			int age = Integer.parseInt(input.nextLine());

			//Calling the constructor
			embarrassment[i] = new Panda(food, sizeInCM, age);
			
			
		}
		System.out.println("The last Panda's favorite food is: " + embarrassment[embarrassment.length -1].getFood());
		System.out.println("The last Panda's size in CM is: " + embarrassment[embarrassment.length-1].getSizeInCM());
		System.out.println("The last Panda's age is: " + embarrassment[embarrassment.length-1].getAge());
		
		//Update age field of last animal in array
		embarrassment[embarrassment.length -1].setAge(Integer.parseInt(input.nextLine()));
		
		System.out.println("The last Panda's favorite food is: " + embarrassment[embarrassment.length -1].getFood());
		System.out.println("The last Panda's size in CM is: " + embarrassment[embarrassment.length-1].getSizeInCM());
		System.out.println("The last Panda's age is: " + embarrassment[embarrassment.length-1].getAge());

	}
	
	
}
